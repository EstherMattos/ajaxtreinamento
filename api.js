// const printResultToConsole = (requestResult) => {
// 	console.log(requestResult);
// }

console.log('oi');

const parseRequestToJson = (requestResult) => {
	return requestResult.json();
}

const newBook = {
	"book" : {
		"name": "Hiiiiiii",
		"author": "Esther"
	}
}

const myHeaders = {
	"Content-Type": "application/json"
}

const fetchConfig = {
	method: 'POST',
	headers: myHeaders,
	body: JSON.stringify(newBook) 
}

fetch('https://treinamento-api.herokuapp.com/books', fetchConfig)
.then((request) => {
	return request.json();
})
.then((responseAsJson) => {
	console.log(responseAsJson);
} );

