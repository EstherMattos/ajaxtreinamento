const url = 'https://treinamento-api.herokuapp.com';

function deleteBookId(id){
	//requisição
	fetch(`${url}/books/${id}`, {
		method: 'DELETE'
	}).then((resposta) => {
		console.log(resposta);
	}).catch((erro) => {
		alert(erro);
	});
}